
import java.lang.Object;
import java.lang.Number;
import java.math.BigInteger;
import java.util.Random;

public class rsa_algorithm 
{

    /** 
	* The Purpose of this class is to implement methods which is present in the model
	* 
	* @ author DeArvis Troutman 
	* @ author DeArvis Troutman
	*
	*/
	private BigInteger p;
	private BigInteger q;
	private BigInteger n;
	private BigInteger phi;
	private BigInteger e;
	private BigInteger d;
	
	
	private int b = 1024;
	/**
	 * 
	 * @DeArvis Troutman
	 */
	public rsa_algorithm()
	{
				// make p and q large prime numbers that are not equal
				// to each other.
		        
		        Random rand = new Random(); // generate a random number
		        
		        p = BigInteger.probablePrime((b/2),rand);
		        q = BigInteger.probablePrime((b - (b/2)), rand);
		       
		     
//		        System.out.println("p: boolean Prime is " + p.isProbablePrime(1));
//		    	System.out.println("q: boolean Prime is" + q.isProbablePrime(1));
//		    	System.out.println("p bit-length: " + p.bitLength());
//		    	System.out.println("q bit-length: " + q.bitLength());
//		        System.out.println(" ");

		        
		       //Checks if valid p and q values
		       while(p.isProbablePrime(1) == false && q.isProbablePrime(1) == false && p == q)
		        {
		    	   p = BigInteger.probablePrime(512, rand);
		    	   q = BigInteger.probablePrime(512, rand);		
	   
//		    	   System.out.println("p: boolean Prime" + p.isProbablePrime(1));
//		    	   System.out.println("q: boolean Prime" + q.isProbablePrime(1));
//		    	   System.out.println("p: Out of Loop" + p);
//		    	   System.out.println("q: Out of Loop " + q);
		        }
		       
		       //n = p * q
		       n = p.multiply(q);
		        
		       //phi = (p-1)(q-1)
		       phi = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		        
		       //Common choices for e are 3, 5, 17, 257 and 65537 (216+1), Fx=2^(2^x)+1
		       e = BigInteger.valueOf(3);
		             
		       //Ensure e is valid
		       validE(e);
		       
		     
		        //(Secret Key) d = multiplication inverse of d,  d * e == 1 modulo (p-1)(q-1) 
		        //d = e^(-1) mod phi
		        d = validE(e).modInverse(phi);
		     
		        //Check for valid d (Should be False)
		        if(d.compareTo(phi) > 0)
		        {
		        	  System.out.println(" 1<d<phi: True " );
		        }
		        
		        if(d.compareTo(phi) < 0)
		        {
		        	   System.out.println(" 1<d<phi: False" ); 
		        }
		     
		        //The encryption function is encrypt(T) = (T^E) mod PQ, where T is the plaintext 
//		        (a positive integer) and '^' indicates exponentiation. 
		        
//				The decryption function 
//		        is decrypt(C) = (C^D) mod PQ, where C is the ciphertext (a positive integer)
//		        and '^' indicates exponentiation.
		        
		    	//messageLength = 5
				//c = m^e mod n.
	}

	
	
public BigInteger getN() 
	{
		return n;
	}


public BigInteger getD() 
	{
		return d;
	}



	//	Check if prime; you only need to check if the number is odd and 
//	is not divisible by any number less than or equal  .
//	If not, increment the number by two and check again. 
	public boolean primeChecker(BigInteger number) {
	    if (!number.isProbablePrime(5))
	        return false;

	    BigInteger two = new BigInteger("2");
	    if (!two.equals(number) && BigInteger.ZERO.equals(number.mod(two)))
	        return false;

	    for (BigInteger i = new BigInteger("3"); i.multiply(i).compareTo(number) < 1; i = i.add(two)) {
	        if (BigInteger.ZERO.equals(number.mod(i)))
	            return false;
	    }
	    return true;
	}
	
	
	
	
	
	
	
	
	public BigInteger validE(BigInteger e)
	{
		
		BigInteger[] ePossibleValues = new BigInteger[]{BigInteger.valueOf(3),BigInteger.valueOf(5),
			       BigInteger.valueOf(17),BigInteger.valueOf(257),BigInteger.valueOf(65537)};
		//Ensure e is valid
	       // 1 < e < phi, such that gcd(e, phi) = 1.
	       while((e.gcd(phi)).equals(BigInteger.ONE) == false)
	       {
	    	   for(int i = 0; i < ePossibleValues.length; i++)
	    	   {
	    		 e = ePossibleValues[i];	  
	    		 e.gcd(phi);
	    		 	if((e.gcd(phi)).equals(BigInteger.ONE))
	    		 	{
	    		 		return e;
	    		 	}
	    	  }
	    	  	 	  
	       }
		return e;
	}
	
	
	
	
	
	
	// takes in the mValues (array of ascii values of all the char's in the message) 
	// And length of message to encrypt
	public BigInteger[] encrypt(int mValues[],int mesgLength)
	{
		BigInteger cipherText[] = new BigInteger[mesgLength];
		BigInteger c;
		BigInteger m;  // m = that char's ascii value 
		
		
		//Iterate through every char in the mesg
		for(int i =0; i < mesgLength;i++ )
		{
			m = BigInteger.valueOf(mValues[i]);
			
			//Computes the ciphertext c = m^e mod n.
			c = m.modPow(validE(e), n);
			cipherText[i] = c;
			
			//Print's out the cipher Text for each char in the following for loop
		}
			
			return cipherText;
	}
	
	
	
	
	
	
	public String decrypt(BigInteger cipherText[],int  mesgLength)
	{
		int intVal;
		char decryptedChar;
		String decryptedVal = "";
		StringBuilder sb = new StringBuilder();
		BigInteger decryption;
		//Uses his private key (n, d) to compute m = c^d mod n
		for(int i =0; i < mesgLength;i++)
		{
			//Finds ASCII Equivalent of char(Decrypts char here.../Private key)
			decryption = cipherText[i].modPow(d, n); 
			

			//convert BigInt ASCII Equivalent to an int
				intVal = decryption.intValueExact();
			
			//convert ASCII Equivalent int to it's string equivalent ex. 97 = a
				decryptedChar = (char)intVal;
				sb.append(decryptedChar);
		}		
		decryptedVal = sb.toString();
		
		return decryptedVal;
	}
	
	
}
