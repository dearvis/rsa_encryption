import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class buttonListener extends MouseAdapter
{
/**
 * This class links the JButtons to the ButtonListener, validating
 * the methods and ensuring that when the button or any action has occured
 * a method is passed.
 * 
 *@ author DeArvis Troutman 
 *@ author DeArvis Troutman
 * 
 * September 28th at Midnight
 * 
 * The information required to run this class ismyController,myMethod and 
 * myArguments.
 */
	
    //////////////////////////
    //     Properties       //
    //////////////////////////
    
    private Object myController;
    private Method myMethod;
    private Object[] myArguments;
    
    //////////////////////////
    //       Methods        //
    //////////////////////////

    /**
     * Only constructor, giving the method that should be invoked
     * when the mouse is released over the button; has the 
     * responsibility of notifying controller when mouse released.
     *
     * @param controller the controller this listener has a
     *        responsible to
     * @param method the method to invoke when button is pushed
     * 
     * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * September 28th at Midnight
     */
    public buttonListener(Object controller, Method method,Object[] args)
    {
        myController = controller;
        myMethod = method;
        myArguments = args;
    }

    /**
     * Invokes the appropriate method when the mouse
     * button is released.
     *
     * <pre>
     * pre:  a valid MouseEvent has taken place, and the controller
     *       and method this listener is responsible to and how, are
     *       set to value values
     * post: the associated method is invoked
     * </pre>
     *
     * @param event a mouse event
     */
    
    public void mouseReleased(MouseEvent event)
    {
        Method method;
        Object controller;
        Object[] arguments;
        
        method = this.getMethod();
        controller = this.getController();
        arguments = this.getArguments();
        
        try
        {
            method.invoke(controller,arguments);
        }
        catch(InvocationTargetException exception)
        {
            System.out.println("InvocationTargetException");
        }
        catch(IllegalAccessException exception)
        {
            System.out.println("IllegalAccessException");
        }
        catch(IllegalArgumentException exception)
        {
            System.out.println("IllegalArgumentException");
        }
    }
  
    //////////////////////////
    //   Accessor Methods   //
    //////////////////////////

    protected Method getMethod()
    {
        return myMethod;
    }

    protected void setMethod(Method method)
    {
        myMethod = method;
    }

    protected Object getController()
    {
        return myController;
    }
      
    protected void setController(Object controller)
    {
        myController = controller;
    }
    
    protected Object[] getArguments()
    {
        return myArguments;
    }
    
    protected void setArguments(Object[] arguments)
    {
        myArguments = arguments;
    }
}

