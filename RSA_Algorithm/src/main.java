
public class main {
		private controller myController;
	
		public static void main(String[] args) 
		{
				new main();
		}
		
		/**
		 * This method initializes the controller.
		 * 
		 * @DeArvis Troutman
		 */
		public main()
	    {
	        this.setController(new controller());
	    }
		public void setController(controller myController) {
			this.myController = myController;
		}

		public controller getController() {
			return myController;
		}
	}

