import java.lang.reflect.Method;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.*;




public class view {
	    public JFrame frame = new JFrame("RSA Algorithm");
	    public  JPanel pane = new JPanel();
		
	    
	    public JFrame wordFrame = new JFrame("Name of Word Document");
	    public JPanel wordPane = new JPanel();
		public JLabel label = new JLabel("Please Enter name of Word Document Below");
	    public JTextArea nameDoc = new JTextArea(1,40);
	    public JButton ok = new JButton("Ok");
		
	    
		public JButton encrypt = new JButton("Encrypt");
		public JButton decrypt = new JButton("Decrypt");
		public JButton load = new JButton("Load");
		public JButton save = new JButton("Save");
		
		public JTextArea message = new JTextArea("Please Enter Message Here...");
		public JTextArea ascii = new JTextArea("ASCII Equivalent will be displayed here...");
		public JTextArea encryptDecrypt = new JTextArea("Encrypted Text/Decrypted Text"
				+ " will be displayed here...");
		
		public GridBagLayout c = new GridBagLayout();
		final static boolean shouldFill = true;
		final static boolean shouldWeightX = true;
		public FlowLayout flow = new FlowLayout();
		
		private buttonListener encryptListener;
		private buttonListener decryptListener;
		private buttonListener loadListener;
		private buttonListener saveListener;
		private buttonListener okListener;
	
		
	public view(controller controller)
	{
		    frame.setSize(550, 450);
		    wordFrame.setSize(500, 150);
		 //   nameDoc.setSize(400, 300);
		   
		    wordPane.setSize(480,130);
		    wordPane.setLayout(flow);
		    
			pane.setSize(450,450);
		    pane.setLayout(c);
			associateListeners(controller);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			wordFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			 wordPane.add(label);
			 wordPane.add(nameDoc);
			 wordPane.add(ok);
			 wordFrame.add(wordPane);
			    
			
			GridBagConstraints c = new GridBagConstraints();

			message.setLineWrap(true);
			message.setWrapStyleWord(true);
			ascii.setLineWrap(true);
			ascii.setWrapStyleWord(true);
			encryptDecrypt.setLineWrap(true);
			encryptDecrypt.setWrapStyleWord(true);
			
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 0;
			c.ipady = 70;      //make this component tall
			c.ipadx = 230;     // make component long (<---->)
			c.insets = new Insets(0,0,10,10);  //top padding
		    JScrollPane Js = new JScrollPane(message,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			pane.add(Js, c);

			
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 1;
		    c.ipadx = 5;
			c.ipady = 5;
			c.insets = new Insets(0,0,10,0);
			pane.add(encrypt, c);

			
			
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 2;
			c.ipadx = 5;
			c.ipady = 5;
			c.insets = new Insets(0,0,0,0);
			pane.add(decrypt, c);


			
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = 2;
			c.ipadx = 5;
			c.ipady = 5;
			c.insets = new Insets(0,0,0,0);
			pane.add(load, c);
			
			
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = 1;
			c.ipadx = 5;
			c.ipady = 5;
			c.insets = new Insets(0,0,0,0);
			pane.add(save, c);
			
			c.ipadx = 250;      //make this component tall
			c.ipady = 170; 
//			c.gridwidth = 3;
			c.gridx = 0;
			c.gridy = 3;
			c.insets = new Insets(0,10,0,0);
			JScrollPane Js2 = new JScrollPane(ascii,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			pane.add(Js2, c);

			
			//c.fill = GridBagConstraints.VERTICAL;
			c.ipady = 170;       //reset to default
			c.ipadx = 200; 
			c.gridx = 1;       //aligned with button 2
			c.gridy = 3;       //third row
			c.insets = new Insets(0,10,0,0);
			JScrollPane Js3 = new JScrollPane(encryptDecrypt,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			pane.add(Js3, c);	
			
			encrypt.addMouseListener(encryptListener);
			decrypt.addMouseListener(decryptListener);
			save.addMouseListener(saveListener);
			load.addMouseListener(loadListener);
			ok.addMouseListener(okListener);
			 
			 frame.add(pane);
		     frame.setResizable(true);	
			 frame.setVisible(true);
			 pane.setVisible(true);
		
		
}
	

	/**
	 * This method links the JButtons with the ButtonListener and mouseListener.
	 * It it correlates the designated buuton with the designated method in the 
	 * controller class. 
	 * 
	 * @param controller
	 * 
	 * @DeArvis Troutman
	 */
	public void associateListeners(controller controller)
	{
        Class<? extends controller> controllerClass;
        Method encryptMethod, decryptMethod, saveMethod, loadMethod, okMethod;
               
      
        controllerClass = controller.getClass();
        
        encryptMethod = null; 
        decryptMethod = null;
        saveMethod = null;
        loadMethod = null;
        okMethod = null;
   
        // Associate method names with actual methods to be invoked
        try
        {
           encryptMethod = controllerClass.getMethod("encrypt",(Class<?>[])null);
           decryptMethod = controllerClass.getMethod("decrypt",(Class<?>[])null); 
           saveMethod = controllerClass.getMethod("save",(Class<?>[])null); 
           loadMethod = controllerClass.getMethod("load",(Class<?>[])null);
           okMethod = controllerClass.getMethod("wordFilename",(Class<?>[])null);
        }
        catch(NoSuchMethodException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
         
        }
        catch(SecurityException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
        }
        
        // Set up listeners with actual argument values passed in
        // to methods
        
//      mySolveListener = new ButtonListener(controller, solveMethod,null);
        encryptListener = new buttonListener(controller, encryptMethod,null);
        decryptListener = new buttonListener(controller, decryptMethod,null);
		loadListener = new buttonListener(controller, loadMethod,null);
		saveListener = new buttonListener(controller, saveMethod,null);
		okListener = new buttonListener(controller,okMethod,null);
}
}