import java.math.BigInteger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class controller {

	/**
	 * This class is connected to the model and the view,
	 *  linking the two with each other and notifying the 
	 *  other when an action or method is implemented.
	 *  
	 * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * September 28th at Midnight
	 * 
	 */
	  ///////////////////
     //  Properties   //
     ///////////////////
     private view myView;
     private rsa_algorithm myModel;
     private String mesg ;
     private StringBuilder sb;
     private StringBuilder sbCypher;
     // Cypher-Text
     private BigInteger[] cT;
     
     private String ascString = null;
     private int mValues[];
     private int messageLength;
     private String filename = " ";
     private boolean saveChecker = false;
     
    
     ///////////////////
     //    Methods    //
     ///////////////////
    
     /**
     * Controller constructor; view must be passed in since 
     * controller has responsibility to notify view when 
     * some event takes place.
     * 
     * @DeArvis Troutman
     */
    public controller()
    {
        myModel = new rsa_algorithm();
        myView = new view(this);
    }
    
    /**
     * This method clears all information in the three JText Area's
     * 
     * @DeArvis Troutman
     */
    public void encrypt()
    {
            // Converting Message to ASCII Values
    	    mesg = myView.message.getText();
            sb = new StringBuilder();
            sbCypher = new StringBuilder();
            mValues = new int[mesg.length()];
            messageLength = mesg.length();
            cT = new BigInteger[messageLength];
        	String encryptedChar;
    		String encryptionVal;
            
    		
            //Going through every char (including space) in the message
                    for (int i = 0; i < mesg.length(); i++)
                    {
                        sb.append((int)mesg.charAt(i));
                        mValues[i] = (int)mesg.charAt(i);
                    //  System.out.println("m-value being added to the array: " + mValues[i]);
                    }
                    
                    // Converted message to ASCII
                     ascString = sb.toString();
                  
                     
                     myView.ascii.setText(ascString);
                     cT = myModel.encrypt(mValues, messageLength);
                   
                     //Convert cipher text to string
                     for(int i =0; i < messageLength;i++ )
                     {
                    	 //convert every cipher to a large string
                    	 encryptedChar = cT[i].toString();	      				
                    	 sbCypher.append(encryptedChar);
                     }
       			
                     // Checks to see if size of cipher is divisible by 5
                     while (sbCypher.length() % 5 != 0)
                     {
                    	 sbCypher.append(0); // Adds 0 to end if cipher is not big enough
                     }
       			
                     int idx = sbCypher.length() - 5;

                     while (idx > 0)
                     {
                    	 sbCypher.insert(idx, " ");
                    	 idx = idx - 5;
                     }
    
                     encryptionVal = sbCypher.toString();	
       	       		 myView.encryptDecrypt.setText(encryptionVal);
        }
   	
    	
    
    /**
     * This method displays the solution on a JLabel on the leftside
     * of the GUI.
     * 
     * @DeArvis Troutman
     */
     public void decrypt()
     {
    	 System.out.println("Decrypting...");
    	 myView.encryptDecrypt.setText( myModel.decrypt(myModel.encrypt(mValues,messageLength), messageLength));
    	 System.out.println("Decrypted");
     }
     
     public void wordFilename()
     {
    	 filename = myView.nameDoc.getText();
    	 System.out.println("FileName: " + filename);
    	 saveChecker = true;
    	 myView.wordFrame.dispose();
    	 this.save();
     }
     
     public void save()
     {
    	 //Private keys for this text
    	 String dVal = myModel.getD().toString();
    	 String nVal = myModel.getN().toString();
    	 
    	 String privateKey = dVal + "," + nVal;
    	 
  
    	 if(saveChecker == false)
    	 {
    		 myView.wordFrame.setResizable(true);
        	 myView.wordFrame.setVisible(true);
        	 myView.wordPane.setVisible(true);
    	 }
        	 
    	 if(saveChecker == true)
    	 {
    		 XWPFDocument document = new XWPFDocument();
    		 XWPFParagraph tmpParagraph = document.createParagraph();
    		 XWPFRun tmpRun = tmpParagraph.createRun();
    		 tmpRun.setText(myView.encryptDecrypt.getText());
    		 System.out.println("Private key: " + privateKey);
    		 
    		
    		 tmpRun.setFontSize(18);
    	     try
    	     {
			    document.write(new FileOutputStream(new File("c:\\Users\\shadow\\desktop\\" + filename + ".docx")));
			    document.close();
		     } 
    	     catch (FileNotFoundException e) 
    	     {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
		     }
    	     catch (IOException e) 
    	     {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
		     }
    	     System.out.println("Saved.");
    	  }
     }
     
     public void load() throws FileNotFoundException, IOException
     {
    	 System.out.println("FILES IN browser");
    	 JFileChooser jf = new JFileChooser();
    	 BufferedReader br;
    	 String currentLine;
    	 File file;
    	 
    	 // This opens file chooser gui, make visible on screen
    	 int returnVal = jf.showOpenDialog(null);
    	
    	 if(returnVal == jf.APPROVE_OPTION) // If choosen file correctly and worked
    	 {
    		 //file = jf.getSelectedFile();
    		 
    		XWPFDocument document = new XWPFDocument(new FileInputStream(jf.getSelectedFile()));
    		XWPFWordExtractor extract = new XWPFWordExtractor(document);
    		myView.message.setText(extract.getText());
    	
    		//Read the file!
//    		 try
//    		 {
//    			 br = new BufferedReader(new FileReader(file));
//    			 while((currentLine = br.readLine()) != null)
//    			 {
//    				// Checks in text file stopping when there is an empty line in the file
//    				 //System.out.println(currentLine);
//    				 myView.message.setText(currentLine);
//    			 }
//    		 } 
//    		 catch (Exception error)
//    		 {
//    			error.printStackTrace();	 
//    	     }
    		 
    	 }
     }
}
